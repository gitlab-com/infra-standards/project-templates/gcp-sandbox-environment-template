# terraform/network.tf

# GCP VPC network for environment resources
resource "google_compute_network" "vpc_network" {
  auto_create_subnetworks = "false"
  description             = "VPC for ${var.env_prefix}"
  name                    = "${var.env_prefix}-vpc"
  routing_mode            = "REGIONAL"
}

# GCP Cloud Router for VPC network
resource "google_compute_router" "router" {
  name    = "${var.env_prefix}-vpc-router"
  network = google_compute_network.vpc_network.name
  region  = var.gcp_region
}

# GCP NAT Gateway for VPC network
resource "google_compute_router_nat" "nat_gateway" {
  name                               = "${var.env_prefix}-vpc-nat-gateway"
  nat_ip_allocate_option             = "AUTO_ONLY"
  router                             = google_compute_router.router.name
  region                             = var.gcp_region
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = "true"
    filter = "ALL"
  }
}

# GCP Subnet for instances and services
# The cidrsubnet function converts the region CIDR of "10.128.0.0/12" into
# a range of "10.128.20.0/24" which is a convention that is used by the
# GitLab Sandbox Cloud. You can customize this to any CIDR range if needed.
resource "google_compute_subnetwork" "region_subnet" {
  name          = "${var.env_prefix}-${var.gcp_region}-subnet"
  ip_cidr_range = cidrsubnet(var.gcp_region_cidr, 12, 20)
  region        = var.gcp_region
  network       = google_compute_network.vpc_network.self_link
  depends_on = [
    google_compute_network.vpc_network
  ]
}

# Firewall rule for Generic Linux Instances
resource "google_compute_firewall" "linux_firewall_rule" {
  name    = "${var.env_prefix}-linux-firewall-rule"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports = [
      "22",
      "80",
      "443",
    ]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["${var.env_prefix}-linux-firewall-rule"]
}

# Firewall rule for GitLab Omnibus instance
# https://docs.gitlab.com/omnibus/package-information/defaults.html
resource "google_compute_firewall" "gitlab_omnibus_firewall_rule" {
  name    = "${var.env_prefix}-gitlab-omnibus-firewall-rule"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports = [
      "22",
      "80",
      "143",
      "443",
      "465",
      "514",
      "5000",
      "5050",
      "5431",
      "5432",
      "6379",
      "6432",
      "8008",
      "8060",
      "8065",
      "8075",
      "8080",
      "8082",
      "8083",
      "8088",
      "8150",
      "8181",
      "8280",
      "8300",
      "8443",
      "8500",
      "9090",
      "9100",
      "9121",
      "9168",
      "9187",
      "9188",
      "9200",
      "26379",
    ]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["${var.env_prefix}-gitlab-omnibus-firewall-rule"]
}
