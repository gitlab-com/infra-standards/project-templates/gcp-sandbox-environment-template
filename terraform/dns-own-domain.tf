# terraform/dns-own-domain.tf

# FAQ: Can I use my own domain name?
# -----------------------------------------------------------------------------
# Yes, however this is not something that we offer support for. You would need
# to register a domain name, create a managed zone, update the registrar with
# the name servers, and then add records. It is not recommended to manage this
# process using this Terraform environment, however if you already have a GCP
# Cloud DNS managed zone in this GCP project, you can add an additional data
# source with your custom domain.

# Custom DNS Zone
# To use a custom DNS zone, this zone must exist inside of this GCP project.
# Change the name of this data source to the name that appears in the web console.
# -----------------------------------------------------------------------------
# data "google_dns_managed_zone" "custom_dns_zone" {
#   name = "my-cool-zone-name"
# }

# -----------------------------------------------------------------------------
# Add custom DNS records below this line
# -----------------------------------------------------------------------------

# Subdomain record for Application Server
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set
# -----------------------------------------------------------------------------
# resource "google_dns_record_set" "gitlab_omnibus_subdomain_dns_record" {
#   managed_zone = google_dns_managed_zone.custom_dns_zone.name
#   name         = "app1."google_dns_managed_zone.custom_dns_zone.dns_name
#   type         = "A"
#   rrdatas      = ["1.2.3.4"]
#   ttl          = 300
# }

# -----------------------------------------------------------------------------
# Add custom DNS records above this line
# -----------------------------------------------------------------------------
