# terraform/variables.tf

variable "env_prefix" {
  type        = string
  description = "A short alphadash slug that is prefixed to resource names when using multiple Terraform configurations in the same GCP project."
  default     = "sandbox"
}

variable "gcp_dns_zone_name" {
  type        = string
  description = "The GCP Cloud DNS zone name for this environment. This is automatically generated and set in the GitLab Project CI variables."
}

variable "gcp_project" {
  type        = string
  description = "The GCP project ID that the resources will be deployed in. This is automatically generated and set in the GitLab Project CI variables."
}

variable "gcp_region" {
  type        = string
  description = "The GCP region that the resources will be deployed in."
  default     = "us-east1"
}

variable "gcp_region_cidr" {
  type        = string
  description = "A /12 CIDR range for the GCP region that will be used for dynamically creating subnets. You can leave this at the default value for most use cases. (Ex. 10.128.0.0/12, 10.144.0.0/12, 10.160.0.0/12)"
  default     = "10.128.0.0/12"
}

variable "gcp_region_zone" {
  type        = string
  description = "The GCP region availability zone that the resources will be deployed in. This must match the region."
  default     = "us-east1-c"
}

variable "labels" {
  type        = map(any)
  description = "Labels to place on the GCP infrastructure resources. Label keys should use underscores, values should use alphadash format. No spaces are allowed. This is automatically generated and set in the GitLab Project CI variables."
  default     = {}
}

# -----------------------------------------------------------------------------
# Add your Terraform custom variables below this line
# You can define non-sensitive values in terraform.tfvars.json
# You can define sensitive values in GitLab CI variables with `TF_VAR_var_name`.
# -----------------------------------------------------------------------------



# -----------------------------------------------------------------------------
# Add your Terraform custom variables above this line
# -----------------------------------------------------------------------------
