# terraform/outputs.tf

# This will return a map with the network configuration.
# This is commented out by default since it is irrelevant information for most users and is only used for debugging.
output "network" {
  value = {
    vpc    = google_compute_network.vpc_network
    router = google_compute_router.router
    nat    = google_compute_router_nat.nat_gateway
    subnet = google_compute_subnetwork.region_subnet
    firewall = {
      linux          = google_compute_firewall.linux_firewall_rule
      gitlab_omnibus = google_compute_firewall.gitlab_omnibus_firewall_rule
    }
  }
}

# -----------------------------------------------------------------------------
# Add your Terraform modules and/or resources below this line
# -----------------------------------------------------------------------------

# This will return a map with all of the outputs for the module
output "experiment_instance" {
  value = module.experiment_instance
}

# If you need a specific key as an output, you can use the dot notation shown above to access the map value.
output "experiment_instance_external_ip" {
  value = module.experiment_instance.network.external_ip
}

# This will return the Fully Qualified Domain Name URL of the resource without the ending `.` (specific to GCP)
output "experiment_instance_dns" {
  value = trimsuffix(module.experiment_instance.dns.instance_fqdn, ".")
}

# -----------------------------------------------------------------------------
# Add your Terraform modules and/or resources above this line
# -----------------------------------------------------------------------------
