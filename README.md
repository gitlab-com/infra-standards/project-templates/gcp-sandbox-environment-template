# GitLab Sandbox Cloud Terraform Environment for GCP

This is a GitLab project (template) that is used for providing the Terraform scaffolding for a GCP project in the GitLab Sandbox Cloud. This is open source for education and forking, however is purpose built for GitLab's internal environments.

When you create a GCP project with the GitLab Sandbox Cloud, a new GitLab project is created with this project template that contains:
* Terraform providers for GCP
* GCP VPC, Subnet, Firewall Rules configuration
* Managed DNS Subdomain Zone for all environment resources (`{emailHandle}-{userId}-{projectId}.gcp.gitlabsandbox.cloud`)
* GitLab CI pipeline for deploying resources without any local machine configuration or credential management needed
* A starter virtual machine using the [gcp-compute-instance-tf-module](https://gitlab.com/gitlab-com/sandbox-cloud/terraform-modules/gcp/gcp-compute-instance-tf-module).

This scaffolding allows team members to create a GCP project and start deploying virtual machines (VM), Kubernetes clusters, and other resources using Terraform without spending several hours getting their environment pre-configured to start using, while maintaining a secure posture for Terraform managed infrastructure. The GitLab Sandbox Cloud automation uses the GitLab API to pre-configuree CI variables with the GCP project credentials, CI configuration and additional Terraform variables that allow team members to start deploying resources quickly.

Each GCP project is associated with one or more Terraform environments that has a 1:1 relationship with a GitLab project.

## Installation Steps

This environment is deployed using GitLab CI in an existing GCP project. See [INSTALL.md](INSTALL.md) for step-by-step build instructions.

## Terraform Commands

This Terraform configuration is managed with GitLab CI for running `terraform plan`, `terraform apply`, and `terraform destroy` tasks. You can review and/or modify the CI jobs in [.gitlab-ci.yml](.gitlab-ci.yml) file.

To run an operation, navigate to `CI/CD > Pipelines` and trigger the appropriate job. All changes to the configuration will automatically run the `Dry Run` stage which will perform a `terraform plan` and `terraform validate`. Be sure to review the results of this pipeline job before running any `Deploy` or `Destroy` stage jobs.

## Connecting to the Environment

| Key | Value |
|-----|-------|
| GCP Project Name | TEMPLATE_GCP_PROJECT_NAME |
| GCP Console URL | [https://console.cloud.google.com/compute/instances?project=TEMPLATE_GCP_PROJECT_NAME&authuser=0](https://console.cloud.google.com/compute/instances?project=TEMPLATE_GCP_PROJECT_NAME&authuser=0) |
| SSH to Experiment Instance | `gcloud beta compute ssh --zone "TEMPLATE_GCP_REGION_ZONE" "experiment-instance"  --project "TEMPLATE_GCP_PROJECT_NAME"`` |
| DNS for Experiment Instance | `experiment-instance.TEMPLATE_ENVIRONMENT_FQDN` |
