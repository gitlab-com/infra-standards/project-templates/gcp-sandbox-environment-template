# .gitlab-ci.yml

# These are the stages for each of the jobs in our pipeline.
stages:
  - Dry Run
  - Deploy
  - Destroy

# This is used by `extends: .terraform` in each of the Terraform stage jobs.
# We do not define these as global values so that they do not interfere with
# other unrelated Terraform jobs that may be added to `.gitlab-ci.yml`, such
# as code quality or Auto DevOps jobs.
# NOTE: It is recommended to use a tagged version number for the image if your
# Terraform configuration uses syntax from v0.13 and earlier.
.terraform:
  image:
    name: hashicorp/terraform:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

#
# Dry Run Stage Jobs
#

🤞 Validate and Plan:
  stage: Dry Run
  extends: .terraform
  script:
    - cd terraform
    - terraform init -backend-config="address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox" -backend-config="lock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="unlock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="username=gitlab-ci-token" -backend-config="password=${CI_JOB_TOKEN}"
    - terraform validate
    - apk --no-cache add jq
    - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
    - terraform plan -out=tfplan.tfplan
    - terraform show -json tfplan.tfplan | convert_report > tfplan.json
  artifacts:
    name: plan
    paths:
      - terraform/tfplan.tfplan
    reports:
      terraform: terraform/tfplan.json
    expire_in: 1 week

#
# Deploy Stage Jobs
#

🚀 Everything:
  stage: Deploy
  extends: .terraform
  script:
    - cd terraform
    - terraform init -backend-config="address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox" -backend-config="lock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="unlock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="username=gitlab-ci-token" -backend-config="password=${CI_JOB_TOKEN}"
    - terraform apply -auto-approve -input=false
  needs: []
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: manual

# 🚀 Omnibus:
#   stage: Deploy
#   extends: .terraform
#   script:
#     - cd terraform
#     - terraform init -backend-config="address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox" -backend-config="lock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="unlock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="username=gitlab-ci-token" -backend-config="password=${CI_JOB_TOKEN}"
#     - terraform apply -auto-approve -input=false -target=module.gitlab_omnibus_sandbox.module.gitlab_omnibus_instance
#   needs: []
#   rules:
#     - if: '$CI_COMMIT_BRANCH == "main"'
#       when: manual

#
# Destroy Stage Jobs
#

💥 Everything:
  stage: Destroy
  extends: .terraform
  script:
    - cd terraform
    - terraform init -backend-config="address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox" -backend-config="lock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="unlock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="username=gitlab-ci-token" -backend-config="password=${CI_JOB_TOKEN}"
    - terraform destroy -auto-approve
  needs: []
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: manual

# 💥 Omnibus:
#   stage: Destroy
#   extends: .terraform
#   script:
#     - cd terraform
#     - terraform init -backend-config="address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox" -backend-config="lock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="unlock_address=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/sandbox/lock" -backend-config="username=gitlab-ci-token" -backend-config="password=${CI_JOB_TOKEN}"
#     - terraform destroy -auto-approve -target=module.gitlab_omnibus_sandbox.module.gitlab_omnibus_instance
#   needs: []
#   rules:
#     - if: '$CI_COMMIT_BRANCH == "main"'
#       when: manual
